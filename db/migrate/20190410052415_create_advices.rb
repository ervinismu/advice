class CreateAdvices < ActiveRecord::Migration[5.2]
  def change
    create_table :advices do |t|
      t.string :name
      t.text :description
      t.datetime :time

      t.timestamps
    end
  end
end
