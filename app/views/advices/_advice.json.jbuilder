json.extract! advice, :id, :name, :description, :time, :created_at, :updated_at
json.url advice_url(advice, format: :json)
