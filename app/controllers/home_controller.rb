class HomeController < ApplicationController
  def index
    render json: {
      status: 'OK',
      results: Advice.select('id, name, description, time'),
      errors: nil
    }, status: 200
  end
end
